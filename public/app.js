
var app = new Vue({
    el: '#app',
    data: {
        input: ''
    },
    computed: {
        output: function () {
            if (this.input.length !== 12) {
                return '';
            }
            var digits = this.input.split('');
            var sum = digits.reverse().map(function(digit, i) {
                return (i+2) * parseInt(digit);
            }).reduce(function(total, value) {
                return total + value;
            });
            var lastDigit = (11 - sum % 11) % 10;

            return this.input + lastDigit;
        }
    }
})